{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        # packages.hello = pkgs.hello;
        devShell = pkgs.mkShell {
          buildInputs = [
          # pkgs.cowsay#pkgs.hello
          pkgs.clojure
          # pkgs.stack
          # pkgs.cabal-install
          ];
          # VAMSHI = "${pkgs.hello}";
          # PKG_CONFIG_PATH = "${pkgs.SDL2}";
          
        };
      });
}

